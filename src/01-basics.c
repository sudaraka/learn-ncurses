#include <stdio.h>
#include <ncurses.h>

int main(int argc, char **argv) {
  // Initialize NCURSES environment.
  initscr();

  // Print a string (const char *) to current window (screen)
  printw("NCURSES Yo!");

  // Update the screen to reflect changes in memory buffer.
  refresh();

  int ch = getch();

  // Cleanup NCURSES environment.
  endwin();

  printf("Existing NCURSES by pressing %d\n", ch);

  return 0;
}
