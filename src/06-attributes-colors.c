#include <stdio.h>
#include <ncurses.h>

/**
 * Attribute values
 *
 * A_NORMAL
 * A_STANDOUT
 * A_REVERSE
 * A_BLINK
 * A_DIM
 * A_BOLD
 * A_PROTECT
 * A_INVIS
 * A_ALTCHARSET
 * A_CHARTEXT
 *
 * Color values
 *
 * COLOR_PAIR(n)
 * COLOR_BLACK    0
 * COLOR_RED      1
 * COLOR_GREEN    2
 * COLOR_YELLOW   3
 * COLOR_BLUE     4
 * COLOR_MAGENTA  5
 * COLOR_CYAN     6
 * COLOR_WHITE    7
 *
 */

int main(int argc, char **argv) {
  // Initialize NCURSES environment.
  initscr();

  int attr = A_REVERSE;

  // Only set color attributes if the terminal supports color.
  if(has_colors()) {
    start_color();

    // Unique color pair (FG/BG) id
    int color_id = 1;

    // Create color pair
    init_pair(color_id, COLOR_RED, COLOR_BLACK);

    // Append color to `attr` value.
    // NOTE: You can also use `attron(COLOR_PAIR(color_id))` instead.
    /* attron(COLOR_PAIR(color_id)); */
    attr |= COLOR_PAIR(color_id);

    // NOTE: following did not work on my first attempt.
    /* if(can_change_color()) { */
    /*   int custom_color_id = 99;  // something not used */
    /*   int tmp_color_id = 2; */
    /*  */
    /*   for(int i = 0; i < 999; i++) { */
    /*     init_color(custom_color_id, 0-i, 0-i, 0-i); */
    /*  */
    /*     init_pair(tmp_color_id, custom_color_id, COLOR_BLACK); */
    /*  */
    /*     attron(A_REVERSE | COLOR_PAIR(tmp_color_id)); */
    /*     printw(" "); */
    /*     attroff(A_REVERSE | COLOR_PAIR(tmp_color_id)); */
    /*   } */
    /*  */
    /*   printw("\n"); */
    /* } */
  }

  attron(attr);

  // Print a string (const char *) to current window (screen)
  printw("NCURSES Yo!");

  attroff(attr);

  getch();

  // Cleanup NCURSES environment.
  endwin();

  return 0;
}
